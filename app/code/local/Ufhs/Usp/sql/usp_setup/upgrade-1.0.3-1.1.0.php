<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('usp/attribute'),'image',array(
	'type' => 'text',
	'nullable' => true,
	'length' => 255,
	'default' => null,
	'comment' => 'Custom Icon'
));

$installer->endSetup();