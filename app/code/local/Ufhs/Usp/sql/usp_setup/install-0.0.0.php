<?php
$installer = $this;
$installer->startSetup();
$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('usp/map')};
	CREATE TABLE {$installer->getTable('usp/map')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`prod_id` int(11) unsigned NOT NULL,
	`attrib_id` int(11) unsigned NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	DROP TABLE IF EXISTS {$installer->getTable('usp/attribute')};
	CREATE TABLE {$installer->getTable('usp/attribute')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`name` varchar(255) NOT NULL,
	`group_id` int(11) unsigned NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	DROP TABLE IF EXISTS {$installer->getTable('usp/attributegroup')};
	CREATE TABLE {$installer->getTable('usp/attributegroup')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	");
$installer->endSetup();