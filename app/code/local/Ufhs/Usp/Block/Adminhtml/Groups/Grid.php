<?php
class Ufhs_Usp_Block_Adminhtml_Groups_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('uspGroupsGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('usp/attributegroup')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('usp')->__('ID'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id'
		));
		$this->addColumn('name', array(
			'header' => Mage::helper('usp')->__('Name'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'name'
		));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("usp_block_adminhtml_groups_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/editgroup', array('id' => $row->getData()['id']));
	}

	public function getEmptyText()
	{
		return $this->__('No groups have been created yet.');
	}
}