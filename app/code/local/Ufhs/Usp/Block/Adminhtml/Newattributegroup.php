<?php
class Ufhs_Usp_Block_Adminhtml_Newattributegroup extends Mage_Adminhtml_Block_Widget_Form_Container
{
	protected function _prepareLayout()
	{
		return parent::_prepareLayout();
	}

	public function __construct()
	{
		parent::__construct();
		$id = $this->getRequest()->getParam('id');
		$this->_objectId = 'id';
		$this->_blockGroup = 'usp';
		$this->_controller = 'adminhtml';
		$this->_mode = 'newattributegroup';

		$this->_removeButton('save');
		$this->_removeButton('delete');
		$this->_removeButton('back');
		$this->_removeButton('reset');

		$this->addButton('new_back', [
			'label' => 'Back',
			'onclick' => "setLocation('" . $this->getUrl('*/*/viewattribgroup') . "')",
			'class' => 'back'
		]);

		$this->addButton('new_save', [
			'label' => 'Save',
			'onclick' => "document.getElementById('newattributegroup').submit()",
			'class' => 'add'
		]);
	}

	public function getHeaderText()
	{
		return Mage::helper('usp')->__('New Attribute Group');
	}
}