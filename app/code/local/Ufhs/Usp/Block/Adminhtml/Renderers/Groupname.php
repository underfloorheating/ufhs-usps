<?php
class Ufhs_Usp_Block_Adminhtml_Renderers_Groupname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		$group = Mage::getModel('usp/attributegroup')->load($value);
		return $group->getName();
	}
}