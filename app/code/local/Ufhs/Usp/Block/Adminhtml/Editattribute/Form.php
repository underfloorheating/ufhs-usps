<?php
class Ufhs_Usp_Block_Adminhtml_Editattribute_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('Edit Attribute');
	}

	protected function _prepareForm()
	{
		$id = $this->getRequest()->getParam('id');
        $attribute = Mage::getModel('usp/attribute')->load($id);

		$form = new Varien_Data_Form(array(
			'id' => 'editattribute',
			'action' => $this->getUrl('*/*/addattrib', array('id' => $this->getRequest()->getParam('id'))),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		));
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('editattrib_form', array(
			'legend' => Mage::helper('usp')->__('Attribute Details')
		));

		$fieldset->addField('name', 'text', array(
			'label' => Mage::helper('usp')->__('Name'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'name',
			'value' => $attribute->getName()
		));

		$fieldset->addField('group_id', 'select', array(
			'label' => Mage::helper('usp')->__('Group'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'group_id',
			'values' => Mage::getModel('usp/attributegroup')->getCollection()->toOptionArray(),
			'value' => $attribute->getGroupId()
		));

		$fieldset->addField('image', 'file', array(
			'label' => Mage::helper('usp')->__('New Icon'),
			'required' => false,
			'name' => 'image',
			'after_element_html' => '<div style="padding-top: 2px;"><small>Icons will be rendered at 18px wide.</small></div>'
		));

		return parent::_prepareForm();
	}
}