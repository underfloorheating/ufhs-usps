<?php
class Ufhs_Usp_Block_Adminhtml_Groups extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_groups';
		$this->_blockGroup = 'usp';
		$this->_headerText = Mage::helper('usp')->__('Groups');
		parent::__construct();
		$this->_updateButton('add', 'onclick',"setLocation('" . $this->getUrl('*/*/newgroup') . "')");
	}
}