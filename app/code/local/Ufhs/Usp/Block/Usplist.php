<?php

class Ufhs_Usp_Block_Usplist extends Mage_Core_Block_Template
{
	public function getUsps($id = '') {
		if (Mage::app()->getRequest()->getControllerName() == "product") {
			$id = Mage::registry('current_product')->getId();
		}
		if (Mage::app()->getRequest()->getControllerName() == "category") {
			$id = $this->getProduct()->getId();
		}
		$collection = Mage::getModel('usp/map')->getCollection()->addFieldToFilter('prod_id', $id);

		$attributeIds = [];
		foreach ($collection as $item) {
			$attributeIds[] = $item->getAttribId();
		}

		return array_map(function($item){
			return Mage::getModel('usp/attribute')->load($item)->getData();
		}, $attributeIds);
	}
}

// class
// text
// img