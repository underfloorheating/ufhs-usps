<?php

class Ufhs_Usp_Model_Observer
{
	/**
	 * Flag to stop observer executing more than once
	 *
	 * @var static bool
	 */
	static protected $_singletonFlag = false;

	/**
	 * This method will run when the product is saved from the Magento Admin
	 * Use this function to update the product model, process the
	 * data or anything you like
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function saveProductTabData(Varien_Event_Observer $observer)
	{
		if (!self::$_singletonFlag) {
			self::$_singletonFlag = true;

			$product = $observer->getEvent()->getProduct();

			try {
				$prodId = $product->getId();
				$tmpBefore = Mage::getModel('usp/map')
				->getCollection()
				->addFieldToFilter('prod_id',$prodId)
				->addFieldToSelect('attrib_id')
				->getData();
				$before = [];
				foreach($tmpBefore as $item) {
					$before[] = (int)$item['attrib_id'];
				}
				$after = empty(Mage::app()->getRequest()->getParam('usp')) ? [] : array_keys(Mage::app()->getRequest()->getParam('usp'));

				if(count($before) != 0 || count($after) != 0) {
					// If it was there before but isn't now, delete it
					foreach ($before as $item) {
						if (!in_array($item, $after)) {
							$collection = Mage::getModel('usp/map')->getCollection()->addFieldToFilter('prod_id', $prodId)->addFieldToFilter('attrib_id', $item);
							foreach ($collection as $map) {
								$map->delete();
							}
						}
					}

					// If it is there now but wasn't before, add it
					foreach ($after as $item) {
						if (!in_array($item, $before)) {
							Mage::getModel('usp/map')
							->setProdId($prodId)
							->setAttribId($item)
							->save();
						}
					}
				}

			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
	}

	private function getProduct()
	{
		return Mage::registry('product');
	}

	private function _getRequest()
	{
		return Mage::app()->getRequest();
	}

	public function categoryLayoutHook(Varien_Event_Observer $observer)
	{
		$render = $observer->getRender();
		$block = $render->getLayout()->createBlock('Ufhs_Usp_Block_Usplist', 'usplist', ['template' => 'usp/category.phtml']);
		$render->getLayout()->getBlock('category_block')->append($block);
	}
}
