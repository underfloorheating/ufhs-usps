<?php
class Ufhs_Usp_Model_Resource_Attributegroup_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('usp/attributegroup');
	}
}