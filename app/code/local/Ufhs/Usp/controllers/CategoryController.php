<?php

require_once('Mage/Catalog/controllers/CategoryController.php');

class Ufhs_Usp_CategoryController extends Mage_Catalog_CategoryController
{
	public function renderLayout($options = '')
	{
		Mage::dispatchEvent("before_catalog_category_render", ['render' => $this]);
		parent::renderLayout();
	}
}